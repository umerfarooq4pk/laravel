<?php
namespace App\Http\Controllers\Api\Auth;
use App\Http\Controllers\Controller;
use Firebase\JWT\JWT;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Models\User;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

use Mail;

class AuthController extends Controller
{

    public function index(){
        return User::with('roles')->get();
    }
    public function signup(Request $request, $company=null)
    {
        $request->validate([
            'email' => 'required|string|email|unique:users',
            'password' => 'required|string|confirmed',
        ]);
        
        $user = new User();
        $user->email = $request->email;
        $user->password = bcrypt($request->password);
        $user->save();

        $data = array(
            'user_id' => md5($user->id)
        );
        $email = $request->email;
        Mail::send('mail', $data, function($message) use ($email) {
            $message->to($email, 'ToDo App')->subject
               ('Activate Your Account');
            $message->from('umerfarooq4pk@gmail.com','Umer Farooq');
         });
        return response()->json([
            'message' => 'Successfully created user!'
        ], 201);
    }
    public function login (Request $request) {
        $request->validate([
            'email' => 'required|email',
            'password' => 'required|string'
        ]);
        $user = User::where('email', $request->email)->first();
        $errors = [];
        if($user && $user->email_verified_at == ''){
            $errors['errors']['activated'][] = 'Activate your account first';
            return response($errors, 422);
        }else if ($user) {
            if (Hash::check($request->password, $user->password)) {
                $tokenResult = $user->createToken('authToken');
                $token = $tokenResult->token;
                $token->save();
                return response()->json([
                    'user' => $user,
                    'access_token' => $tokenResult->accessToken,
                    'token_type' => 'Bearer',
                    'expires_at' => Carbon::parse(
                        $tokenResult->token->expires_at
                    )->toDateTimeString()
                ]);
            } else {
                $errors['errors']['password'][] = 'password is not correct';
                return response($errors, 422);
            }
        } else {
            $errors['errors']['email'][] = 'Email does not exist';
            return response($errors, 422);
        }
    }

    public function logout(Request $request)
    {
        $request->user()->token()->revoke();
        return response()->json([
            'message' => 'Successfully logged out'
        ]);
    }

    public function user(Request $request)
    {
        return response()->json($request->user());
    }

    public function activate($id){
        $user = User::whereRaw('md5(id) = "' . $id . '"')->update(['email_verified_at' => now()]);
        echo '<h1>Your account is activated, you can login now on application</h1>';
    }
}
