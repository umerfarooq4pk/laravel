<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class checkUser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $user = $request->headers->get('user_id');
        if (!$user){
            $errors = array();
            $errors['errors']['not_fount'][] = 'User can not found';
            return response($errors, 422);
        }
        if ($user && Auth::user()->id == $user){
            return $next($request);
        }
        return response(['message'=> 'You are not authorized to perform this action'],422);
    }
}
